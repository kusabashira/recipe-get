package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/ymotongpoo/goltsv"
)

func version() {
	os.Stderr.WriteString(`recipe-get: v0.2.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: recipe-get [OPTION] RECIPE_FILE
Download files using LTSV recipe file

Options:
	--dir -d     Directory of the download destination
	--help       show this help message and exit
	--version    print the version and exit

Report bugs to <kusabashira227@gmail.com>
`)
}

func trimHeadSpaceAndTailSpace(s string) string {
	return strings.TrimLeft(strings.Trim(s, " "), " ")
}

type Recipe []map[string]string

func readRecipe(fileName string) (recipe Recipe, err error) {
	ltsvFile, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer ltsvFile.Close()

	ltsvReader := goltsv.NewReader(ltsvFile)
	recipe, err = ltsvReader.ReadAll()
	if err != nil {
		return nil, err
	}

	return recipe, nil
}

func downloadFile(fileName, downloadURL string) error {
	res, err := http.Get(downloadURL)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, res.Body)
	if err != nil {
		return err
	}

	return nil
}

func downloadWithRecipe(recipe Recipe) error {
	log.Println("INFO: start downloading")
	for _, record := range recipe {
		for fname, downloadURL := range record {
			fname = trimHeadSpaceAndTailSpace(fname)
			downloadURL = trimHeadSpaceAndTailSpace(downloadURL)

			err := downloadFile(fname, downloadURL)
			if err != nil {
				log.Printf("ERRO: failed downloading `%s`\n",
					fname)
				return err
			} else {
				log.Printf("INFO: saved `%s`\n", fname)
			}
		}
	}
	log.Println("INFO: finish downloading")
	return nil
}

func _main() (exitCode int, err error) {
	var downloadDir string
	flag.StringVar(&downloadDir, "d", ".",
		"Directory of the download destination")
	flag.StringVar(&downloadDir, "dir", ".",
		"Directory of the download destination")

	var isHelp, isVersion bool
	flag.BoolVar(&isHelp, "help", false,
		"show this help message")
	flag.BoolVar(&isVersion, "version", false,
		"print the version")

	flag.Usage = usage
	flag.Parse()

	if isHelp {
		usage()
		return 0, nil
	}

	if isVersion {
		version()
		return 0, nil
	}

	var recipeFile string
	if flag.NArg() < 1 {
		return 1, fmt.Errorf("no recipe file")
	}
	recipeFile = flag.Arg(0)

	var recipe Recipe
	recipe, err = readRecipe(recipeFile)
	if err != nil {
		return 1, err
	}

	err = os.Chdir(downloadDir)
	if err != nil {
		return 1, err
	}

	err = downloadWithRecipe(recipe)
	if err != nil {
		return 1, err
	}

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "recipe-get:", err)
	}
	os.Exit(exitCode)
}
