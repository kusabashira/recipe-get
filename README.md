recipeget
====================
Download files using LTSV recipe file

License
====================
MIT License

Usage
====================

	$ recipeget [OPTION] RECIPE

	Options:
		--dir -d     Directory of the download destination
		--help       show this help message and exit
		--version    print the version and exit

Example
====================
$ recipeget recipe.ltsv
Download files using recipe.ltsv

$ recipeget -dir=.. modlist.ltsv
Download files using modlist.ltsv to parent directory

$ recipeget -to=C:\dev C:\dev\modlist.ltsv
Download files using C:\dev\modlist.ltsv to C:\dev

Recipe
====================
Describe in [LTSV](http://ltsv.org/) recipe

file name and download URL, colon separated.

	file name : download URL

###Example(recipe.ltsv)

	Vim74.zip:http://files.kaoriya.net/goto/vim74w32
	quickrun.zip:https://github.com/thinca/vim-quickrun/archive/master.zip
	syntastic.zip:https://github.com/scrooloose/syntastic/archive/master.zip

Download file from `http://files.kaoriya.net/goto/vim74w32`.
save as `Vim74.zip`

Download file from `https://github.com/thinca/vim-quickrun/archive/master.zip`.
save as `quickrun.zip`

Download file from `https://github.com/scrooloose/syntastic/archive/master.zip`.
save as `syntastic.zip`

Author
====================
Kusabashira <kusabashira227@gmail.com>
